# Canonicalize Markdown file using Pandoc

Rewrites a Markdown text file to [RStudio/Quarto's canonical Markdown representation](https://quarto.org/docs/visual-editor/markdown.html#canonical-mode), based
on [Pandoc](https://pandoc.org/).

Note that the [`indented_to_backtick_code_blocks.lua`](lua_filters/indented_to_backtick_code_blocks.lua) Pandoc [Lua
filter](https://pandoc.org/lua-filters.html) must be [available to Pandoc](https://pandoc.org/MANUAL.html#option--lua-filter), i.e. ideally copied to
`$DATADIR/filters` (which usually is `~/.local/share/pandoc/filters` on Linux).

## Parameters:

1.  Path to the input file. A string.

2.  Column at which to wrap lines (aka maximum line width). An integer. Set to `0` to disable line wrapping. Cf. https://pandoc.org/MANUAL.html#option--columns

3.  Footnote location. One of `block`, `section` or `document`. Defaults to `document` if not provided. Cf.
    https://pandoc.org/MANUAL.html#option--reference-location

4.  Whether or not to prefix footnote identifiers with a normalized representation of the input filename. Enabled by default. Any value disables this option.
    Cf. https://pandoc.org/MANUAL.html#option--id-prefix

## Requirements:

-   CLI tools:
    -   [coreutils](https://en.wikipedia.org/wiki/List_of_GNU_Core_Utilities_commands)
    -   [`pandoc`](https://pandoc.org/)
    -   [`sd`](https://github.com/chmln/sd)

## Background and further discussion

-   [Original RStudio issue](https://github.com/rstudio/rstudio/issues/14570)
-   [Quarto feature request to expose `quarto canonicalize` subcommand](https://github.com/quarto-dev/quarto-cli/discussions/9476)
-   [Quarto (Visual editor) bug report about imperfect filename normalization](https://github.com/quarto-dev/quarto/issues/424)
