--[[

Pandoc filter to convert Markdown indented code blocks to backtick-fenced code blocks

Source: https://github.com/jgm/pandoc/issues/2120#issuecomment-2077936746

]]

local formats = {
  markdown = true,
  gfm = true,
  commonmark = true,
  commonmark_x = true
}

if not (formats[FORMAT]) then
  error(FORMAT .. " not supported")
end

local md_attrs = {
  'identifier',
  'attributes'
}

CodeBlock = function(elem)
  local text, classes, attr = elem.text, elem.classes, elem.attr
  if classes[1] and 0 < #classes[1] then
    return elem
  elseif 'markdown' == FORMAT then
    for _, key in ipairs(md_attrs) do
      if 0 < #attr[key] then
        return elem
      end
    end
  end
  local wd = 2
  for t in text:gmatch("%`+") do
    if #t > wd then
      wd = #t
    end
  end
  local fence = ('`'):rep(wd + 1)
  local block = fence .. "\n" .. text .. "\n" .. fence
  return pandoc.RawBlock(FORMAT, block)
end
