#!/bin/sh

# Rewrite a Markdown file to [RStudio/Quarto's canonical Markdown representation](https://quarto.org/docs/visual-editor/markdown.html#canonical-mode), based on
# [Pandoc](https://pandoc.org/).
#
# Parameters:
#
# 1. Path to the input file. A string.
#
# 2. Column at which to wrap lines (aka maximum line width). An integer. Set to `0` to disable line wrapping. Cf. https://pandoc.org/MANUAL.html#option--columns
#
# 3. Footnote location. One of `block`, `section` or `document`. Defaults to `document` if not provided.
#    Cf. https://pandoc.org/MANUAL.html#option--reference-location
#
# 4. Whether or not to prefix footnote identifiers with a normalized representation of the input filename. Enabled by default. Any value disables this option.
#    Cf. https://pandoc.org/MANUAL.html#option--id-prefix
#
# Requirements:
#
# - CLI tools:
#   - [coreutils](https://en.wikipedia.org/wiki/List_of_GNU_Core_Utilities_commands)
#   - [`pandoc`](https://pandoc.org/)
#   - [`sd`](https://github.com/chmln/sd)

# basic assertions
## ensure 1st and 2nd arg are provided
if [ -z "$1" ] ; then
  echo "No input file path provided."
  exit 1
fi

if [ -z "$2" ] ; then
  echo "No column width at which to wrap lines provided. Set to \`0\` to disable or a positive integer to enable line wrapping at the respective column."
  exit 1
fi

## ensure at max four args are provided
if [ -n "$5" ] ; then
  echo "More than three arguments provided. This script only accepts up to three."
  exit 1
fi

## ensure all required CLI tools are available
if ! command -v pandoc >/dev/null ; then
  echo "\`pandoc\` is required but not found on PATH. See https://pandoc.org/installing.html"
  exit 1
fi

if ! command -v sd >/dev/null ; then
  echo "\`sd\` is required but not found on PATH. See https://github.com/chmln/sd#readme"
  exit 1
fi

# determine line wrapping mode
if [ "$2" -eq 0 ] ; then
  WRAP='--wrap=none'
  COLUMNS=''
else
  WRAP='--wrap=auto'
  COLUMNS="--columns=$2"
fi

# determine footnote location
if [ -z "$3" ] ; then
  REF_LOC='--reference-location=document'
elif echo "$3" | grep -Eq '^(block|section|document)$' ; then
  REF_LOC="--reference-location=$3"
else
  echo "Footnote location must be one of \`block\`, \`section\` or \`document\`, not \`$3\`."
  exit 1
fi

# determine footnote ID prefix
if [ -z "$4" ] ; then
  ID_PREFIX=--id-prefix=$(basename "$1" | sd '\.\w+$' '' | tr '[:upper:]' '[:lower:]' | sd '\n$' '' | sd '\s' '-' | sd '[^\w\-.–—£§°´(]' '')-
else
  ID_PREFIX=''
fi

# assemble Pandoc CLI command, cf. https://unix.stackexchange.com/a/444949/201803
# NOTES:
# - The pseudo-array `"$@"` mustn't contain empty elements.
# - The [`smart`](https://pandoc.org/MANUAL.html#extension-smart) extension must be disabled both for in- and output to leave quotes etc. untouched.
# - The [`raw_attribute`](https://pandoc.org/MANUAL.html#extension-raw_attribute) extension must be disabled for the output to preserve HTML tags as-is.
# - The [`--standalone`](https://pandoc.org/MANUAL.html#option--standalone) arg is required to retain YAML metadata headers and to avoid adding prefixed ID
#   attributes to all headers.
# - The `--lua-filter=indented_to_backtick_code_blocks.lua` ensures code blocks are always output in backtick-fenced instead of indented syntax.
#   Cf. https://github.com/jgm/pandoc/issues/2120#issuecomment-2077936746
INPUT="$1"
set -- pandoc \
    --from=markdown-smart \
    --to=markdown-smart-raw_attribute-simple_tables-multiline_tables-grid_tables \
    --lua-filter=indented_to_backtick_code_blocks.lua \
    --markdown-headings=atx \
    --standalone \
    "${WRAP}" \
    "${REF_LOC}" \
    "--output=${INPUT}"
if [ -n "${COLUMNS}" ] ; then
  set -- "$@" "${COLUMNS}"
fi
if [ -n "${ID_PREFIX}" ] ; then
  set -- "$@" "${ID_PREFIX}"
fi
set -- "$@" "${INPUT}"

# canonicalize
"$@"
