#!/bin/bash

# required ubuntu packages: cuetools ffmpeg flac lame mediainfo monkeys-audio shntool wavpack

# arguments:
# -p [path]: path to process (all subdirs under [path] will be processed, too); default is the current dir
# -r: remove the original **unsplit** lossless audio file; unset by default

# handle option arguments
while getopts ":p::r" opt; do
  case $opt in
    p)
      path="$OPTARG"
    ;;
    r)
      rm_unsplit=true
    ;;
    \?)
      echo "Invalid option -$OPTARG" >&2
      exit 1
    ;;
  esac
done

## set defaults
if [[ -z $path ]] ; then path=$PWD; fi
if [[ -z $rm_unsplit ]] ; then rm_unsplit=false ; fi

# define function to transcode lossless audio files under current dir
transcode() {
  if [[ ! -d "MP3" ]] ; then
    count_flac=`ls -1 *.flac 2>/dev/null | wc -l`
    if [[ $count_flac == 1 ]] ; then cued_codec="flac" ; fi
    count_ape=`ls -1 *.ape 2>/dev/null | wc -l`
    if [[ $count_ape == 1 ]] ; then cued_codec="ape" ; fi
    count_wv=`ls -1 *.wv 2>/dev/null | wc -l`
    if [[ $count_wv == 1 ]] ; then cued_codec="wv" ; fi
    count_mp4=`ls -1 *.mp4 2>/dev/null | wc -l`
    count_m4a=`ls -1 *.m4a 2>/dev/null | wc -l`
    if [[ $count_mp4 > 0 || $count_m4a > 0 ]] ; then
      is_alac=true
      if [[ $count_mp4 > 0 ]] ; then cued_codec="mp4" ; elif [[ $count_m4a > 0 ]] ; then cued_codec="m4a" ; fi
      for i in *.$cued_codec ; do
        get_codec=`mediainfo --Inform="General;%Audio_Format_List%" "$i"`
        if [[ $get_codec != "ALAC" ]] ; then is_alac=false ; fi
      done
    fi
    codec_sum=$(($count_flac + $count_ape + $count_wv + $count_mp4 + $count_m4a))
    if [[ $codec_sum > 0 ]] ; then
      count_cue=`ls -1 *.cue 2>/dev/null | wc -l`
      if [[ $codec_sum == 1 && $count_cue > 0 ]] ; then
        fname=`ls *.$cued_codec 2>/dev/null`
        if [[ ($cued_codec == "mp4" || $cued_codec == "m4a") && $is_alac == true ]] ; then
          shntool split -f *.cue -o alac *.$cued_codec -t "%n - %t"
        elif [[ $cued_codec == "wv" ]] ; then
          wvunpack *.wv
          cued_codec="wav"
          fname=`ls *.wav 2>/dev/null`
          shntool split -f *.cue -o wav *.wav -t "%n - %t"
        else
          shntool split -f *.cue -o $cued_codec *.$cued_codec -t "%n - %t"
        fi
        count=`ls -1 *.$cued_codec 2>/dev/null | wc -l`
        if [[ $count > 1 && $rm_unsplit == "true" ]] ; then rm "$fname" ; fi
        codec_sum=`ls -1 *.$cued_codec 2>/dev/null | wc -l`
      fi
      if [[ $count_flac > 0 ]] ; then flac -d *.flac ; fi
      if [[ $count_ape > 0 ]] ; then for i in *.ape ; do mac "$i" "`basename "$i" .ape`".wav -d ; done ; fi
      if [[ $count_wv > 1 || ($count_wv > 0 && $count_cue == 0) ]] ; then wvunpack *.wv ; fi
      if [[ $count_mp4 > 0 && $is_alac == true ]] ; then for f in *.mp4 ; do avconv -i "$f" "${f%.mp4}.wav" ; done ; fi
      if [[ $count_m4a > 0 && $is_alac == true ]] ; then for f in *.m4a ; do avconv -i "$f" "${f%.m4a}.wav" ; done ; fi
      for i in *.wav ; do lame --preset extreme --replaygain-accurate "$i" "`basename "$i" .wav`".mp3 ; done
      if [[ `ls -1 *.wav 2>/dev/null | wc -l` == $codec_sum ]] ; then rm *.wav ; fi
      count_mp3=`ls -1 *.mp3 2>/dev/null | wc -l`
      if [[ $count_mp3 > 0 ]] ; then
        mkdir MP3
        mv *.mp3 MP3
      fi
    fi
  fi
}

# define function to recursively iterate over subdirs and call `transcode()` 
goDeep() {
  transcode
  for dir in "$PWD"/* ; do
    if [[ -d "$dir" ]] ; then
      cd "$dir"
      goDeep
    fi
  done
}

# go to `path` and call `goDeep()`
cd "$path"
goDeep
