# Notes

## TODO

- properly pass position-independent cmdline arguments, either by
    - using [enhanced getopt](https://stackoverflow.com/a/29754866/7196903)
    - or by using [argbash](https://github.com/matejak/argbash)


- improve the `-r` flag: if unset, clean up instead, i.e. delete the split lossless files after conversion


- split the script into:
    1. just the function `transcode()` that converts the contents of a specific directory only
    2. a script like the existing one which makes use of 1)


- programmatically check if the required software tools/packages are installed on the system and throw warnings/instructions if not


- make the path of the output (converted MP3 files) configurable by parameter (like `--output ...` / `-o ...`)


- documentation: create a man page and/or `--help` / `-h` output :grimacing:


## Miscellaneous

### loop over files/file extensions

<https://stackoverflow.com/a/25774685/7196903>

