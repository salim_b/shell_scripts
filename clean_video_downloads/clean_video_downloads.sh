#!/bin/bash

# arguments:
# 1. video type: `series` or `movie` (defaults to `movie`)
# 2. directory (defaults to `.`)

# test if file/folder exists
# inspired by: https://stackoverflow.com/a/24615684/7196903
exists()
{
  [[ -e $1 ]]
}

# extract archive file(s)
# source: https://askubuntu.com/a/338759/622358
extract ()
{
  for archive in "$@" ; do
    if [[ -f "$archive" ]] ; then
      case "$archive" in
        *.tar)      tar xf "$archive"       ;;
        *.tar.bz2)  tar xjf "$archive"      ;;
        *.tbz2)     tar xjf "$archive"      ;;
        *.tar.gz)   tar xzf "$archive"      ;;
        *.tgz)      tar xzf "$archive"      ;;
        *.bz2)      bunzip2 "$archive"      ;;
        *.gz)       gunzip "$archive"       ;;
        *.zip)      unzip "$archive"        ;;
        *.7z)       7z e "$archive"         ;;  # 'p7zip-full' must be installed
        *.Z)        uncompress "$archive"   ;;
        *.rar)      unrar e "$archive"      ;;  # 'unrar' must be installed
        *)          echo "'$archive' cannot be extracted via extract()" ;;
      esac
      rm "$archive"
    else
      echo "'$archive' is not a valid file"
    fi
  done
}

# move a video file up in directory hierarchy
moveVideoFile()
{
  initial_name=`basename "\`ls -1 | grep -i \.$1$\`" .mkv`

  # only rename files once
  if [[ $renamed == true ]]
  then base_name="$initial_name"
  else renamed=true
  fi
  
  if [[ `ls -1 | grep -i \.$1$` != "${move_to_parent_dir}${base_name}.$1" ]]
  then mv `ls -1 | grep -i \.$1$` "${move_to_parent_dir}${base_name}.$1"
  fi
}

# move subtitle files up in directory hierarchy
moveSubtitleFiles()
{
  for subtitle_file in `ls -1 | grep -i \.$1$`
  do
    subtitle_name=`basename "$subtitle_file" .$1`
    subtitle_name="${subtitle_name:$initial_name_length}"

    if [[ $subtitle_name == "" ]]
    then subtitle_name="$base_name"
    else subtitle_name="${base_name}.${subtitle_name:1}"
    fi

    if [[ "$subtitle_file" != "${move_to_parent_dir}${subtitle_name}.$1" ]]
    then mv "$subtitle_file" "${move_to_parent_dir}${subtitle_name}.$1"
    fi
  done
}

# process a directory
processDir()
{
  # delete sample and proof folders
  if basename "$PWD" | grep -Eiq "^(samples?|proofs?)$" ; then rm -R "$PWD"

  # handle subs folder
  elif basename "$PWD" | grep -Eiq "^subs?(titles?)?$"
  then
    # extract subs archive if necessary
    archive_files=( `ls -A1 | grep -Ei '\.(tar(\.(bz2|gz))?|tbz2|tgz|bz2|gz|zip|7z|Z|rar)$'` )
    if [[ $archive_files > 0 ]]
    then extract "${archive_files[@]}"
    fi
    # move subs to parent dir
    if exists *.[iI][dD][xX]
    then mv *.[iI][dD][xX] ../
    fi
    if exists *.[sS][uU][bB]
    then mv *.[sS][uU][bB] ../
    fi
    if exists *.[sS][rR][tT]
    then mv *.[sS][rR][tT] ../
    fi

    # delete subs folder if empty or only .sfv or rushchk.log files left
    if [[ $(( `ls -A1 | wc -l` - `ls -A1 | grep -Eci '(rushchk.log|\.sfv)$'` )) == 0 ]]
    then rm -R "$PWD"
    fi

  # rename, move and delete target files (only if there's exactly one video file in folder)
  elif [[ `ls -1 | grep -Eci '\.(mkv|avi|mp4)$'` == 1 ]]
  then
    base_name=`basename "$PWD"`
    # handle annoying naming scheme used by e.g. https://hdencode.com
    if [[ `echo $base_name | grep -Pcio ".+?(?= ~ )"` > 0 ]]
    then base_name=`echo $base_name | grep -Pio ".+?(?= ~ )"`
    fi

    initial_name=""

    # only move files to parent dir if processing a tv series or parent dir contains only 1 folder and nothing else
    ls_parent_dir=`ls -A1 ../`
    if [[ "$1" == "series" || (( `echo "$ls_parent_dir" | wc -l` == 1 ) && -d ../"$ls_parent_dir" ) ]]
    then move_to_parent_dir="../"
    else move_to_parent_dir=""
    fi
    
    # move video file
    if [[ `ls -1 *.[mM][kK][vV] 2>/dev/null | wc -l` == 1 ]]
    then moveVideoFile mkv
    fi

    if [[ `ls -1 *.[aA][vV][iI] 2>/dev/null | wc -l` == 1 ]]
    then moveVideoFile avi
    fi

    if [[ `ls -1 *.[mM][pP]4 2>/dev/null | wc -l` == 1 ]]
    then moveVideoFile mp4
    fi

    initial_name_length=${#initial_name}

    # move subtitles
    if exists *.[iI][dD][xX] && [[ "$initial_name" != "" ]]
    then moveSubtitleFiles idx
    fi

    if exists *.[sS][uU][bB] && [[ "$initial_name" != "" ]]
    then moveSubtitleFiles sub
    fi

    if exists *.[sS][rR][tT] && [[ "$initial_name" != "" ]]
    then moveSubtitleFiles srt
    fi

    # if not processing a tv series, move .nfo files, too; otherwise delete them
    if exists *.[nN][fF][oO] && [[ "$move_to_parent_dir" != "" ]]
    then
      if [[ "$1" != "series" ]]
      then mv *.[nN][fF][oO] ../
      else rm *.[nN][fF][oO]
      fi
    fi

    # delete .jpg files smaller than 200kB (low-res cover art)
    if exists *.[jJ][pP][gG]
    then
      for jpg in *.[jJ][pP][gG]
      do
        jpg_size=`stat --printf="%s" "$jpg"`

        if [[ $jpg_size -lt 200000 ]]
        then rm "$jpg"
        fi
      done
    fi

    # delete folder if empty or only imdb.html, .sfv, .srr, .txt and/or .url files left
    if [[ $(( `ls -A1 | wc -l` - `ls -A1 | grep -Eci '(imdb\.html|\.(sfv|srr|txt|url))$'` )) == 0 ]]
    then rm -R "$PWD"
    fi
  fi
}

# go recursively through all subdirectories
goDeep()
{
  for path in "$PWD"/*
  do
    renamed=false

    if [[ -d "$path" ]]
    then
      cd "$path"
      goDeep "$1"
      cd ..
    fi
  done

  processDir "$1"
}

if [[ ! -z "$2" ]]
then cd "$2"
fi

goDeep "$1"
