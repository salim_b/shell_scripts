#!/bin/bash

# arguments:
# 1. old filename to rename (incl. suffix)
# 2. new filename (incl. suffix)
# 3. directory (defaults to `.`)

rename()
{
  if [[ -e "$1" ]] ; then
    mv "$1" "$2"
  fi
}

goDeep()
{
  rename "$1" "$2"
  for dir in "$PWD"/* ; do
    if [[ -d "$dir" ]] ; then
      cd "$dir"
      goDeep "$1" "$2"
    fi
  done
}

if [[ ! -z "$3" ]] ; then cd "$3" ; fi

goDeep "$1" "$2"
