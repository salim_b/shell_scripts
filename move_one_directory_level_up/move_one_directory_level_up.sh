#!/bin/bash

# arguments:
# 1. directory (defaults to `.`)
# 2. "del" flag; means to delete the empty subfolders from which the content got moved one level up (disabled by default)

if [[ ! -z "$1" ]] ; then cd "$1" ; fi

for dir in "$PWD"/* ; do
  if [[ -d "$dir" ]] ; then
    cd "$dir"
    for file_dir in * ; do
      parent_dir=`dirname "$PWD"`
      mv "${PWD}/${file_dir}" "${parent_dir}/${file_dir}"
    done
    if [[ "$2" == "del" ]] ; then rm -R "$dir" ; fi
  fi
done
