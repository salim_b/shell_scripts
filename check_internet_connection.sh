#!/bin/bash

if ping -c 3 8.8.8.8 | grep -q "100% packet loss" ; then
  echo "no connection"
  exit 0
else
  echo "all fine"
  exit 1
fi
