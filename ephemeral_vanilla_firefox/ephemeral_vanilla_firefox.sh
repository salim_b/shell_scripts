#!/bin/sh

PROFILEDIR=$(mktemp -p /tmp -d ephemeral_vanilla_ff_profile.XXXXXX)
firefox --profile "$PROFILEDIR" --no-remote --new-instance
rm -rf "$PROFILEDIR"
