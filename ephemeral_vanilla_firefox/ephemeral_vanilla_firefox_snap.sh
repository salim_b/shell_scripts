#!/bin/sh

PROFILEDIR=$(mktemp -p $HOME/Downloads -d ephemeral_vanilla_ff_profile.XXXXXX)
/snap/bin/firefox --profile "$PROFILEDIR" --no-remote --new-instance
rm -rf "$PROFILEDIR"
