#!/bin/sh

PROFILEDIR=$(mktemp -p $HOME/Downloads -d ephemeral_vanilla_ff_profile.XXXXXX)
/usr/bin/flatpak run --branch=stable --arch=x86_64 --command=firefox org.mozilla.firefox --profile "$PROFILEDIR" --no-remote --new-instance
rm -rf "$PROFILEDIR"
