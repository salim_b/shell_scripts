#!/usr/bin/env bash

# source: http://www.webupd8.org/2016/03/translate-any-text-you-select-on-your.html
#
# to use it conveniently define a keyboard shortcut with this file's path as command 

notify-send --icon=info "$(xsel -o)" "$(wget -U "Mozilla/5.0" -qO - "http://translate.googleapis.com/translate_a/single?client=gtx&sl=auto&tl=de&dt=t&q=$(xsel -o | sed "s/[\"'<>]//g")" | sed "s/,,,0]],,.*//g" | awk -F'"' '{print $2, $6}')"

