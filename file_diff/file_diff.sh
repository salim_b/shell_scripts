#!/bin/sh

# [diff](https://en.wikipedia.org/wiki/Diff) augmented with pretty-colored word-by-word highlighting powered by [diffr](https://github.com/mookid/diffr)
#
# args: paths to files to be compared

diff -u "$@" | diffr --colors refine-added:none:background:0x33,0x99,0x33:bold --colors added:none:background:0x33,0x55,0x33 --colors refine-removed:none:background:0x99,0x33,0x33:bold --colors removed:none:background:0x55,0x33,0x33
